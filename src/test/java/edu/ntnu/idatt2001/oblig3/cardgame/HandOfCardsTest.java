package edu.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testclass for the HandOfCards class
 */
class HandOfCardsTest {
    ArrayList<PlayingCard> cardList = new ArrayList<>();

    /**
     * Tests if the sum returned by sumOfHand() is correct
     */
    @Test
    void sumOfHand() {
        cardList.add(new PlayingCard('S',10));
        cardList.add(new PlayingCard('H',7));
        cardList.add(new PlayingCard('C',8));
        cardList.add(new PlayingCard('D',2));
        HandOfCards hand = new HandOfCards(cardList);
        assertTrue(hand.sumOfHand() == (10+7+8+2));
    }

    /**
     * Tests if the cards of hearts returned by cardsOfHearts() is correct
     */
    @Test
    void cardsOfHearts() {
        cardList.add(new PlayingCard('S',10));
        cardList.add(new PlayingCard('H',7));
        cardList.add(new PlayingCard('C',8));
        HandOfCards hand = new HandOfCards(cardList);
        assertTrue(hand.cardsOfHearts().equals("H7"));
    }

    /**
     * Testmethod for queenOfSpades()
     */
    @Test
    void queenOfSpades() {
        cardList.add(new PlayingCard('D',12));
        HandOfCards hand = new HandOfCards(cardList);
        assertFalse(hand.queenOfSpades());
        cardList.add(new PlayingCard('S',12));
        hand = new HandOfCards(cardList);
        assertTrue(hand.queenOfSpades());
    }

    /**
     * Testmethod for flush()
     */
    @Test
    void flush() {
        cardList.add(new PlayingCard('S',10));
        cardList.add(new PlayingCard('S',7));
        cardList.add(new PlayingCard('S',4));
        cardList.add(new PlayingCard('S',3));
        cardList.add(new PlayingCard('S',8));
        HandOfCards hand = new HandOfCards(cardList);
        assertTrue(hand.flush());
        cardList.remove(cardList.get(0));
        cardList.add(new PlayingCard('H',8));
        hand = new HandOfCards(cardList);
        assertFalse(hand.flush());
    }
}