module cardgame {
    requires javafx.controls;
    requires javafx.fxml;
    exports edu.ntnu.idatt2001.oblig3.cardgame;
    exports edu.ntnu.idatt2001.oblig3.cardgame.GUI;
    opens edu.ntnu.idatt2001.oblig3.cardgame.GUI;
    opens edu.ntnu.idatt2001.oblig3.cardgame;
}