package edu.ntnu.idatt2001.oblig3.cardgame.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

/**
 * GUI class that extends the Application class from JavaFX
 */
public class CardgameGUI extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Card Game");

        FXMLLoader loader = new FXMLLoader();
        URL mainPageURL = getClass().getResource("/GUI/cardgame.fxml");
        loader.setLocation(mainPageURL);

        // Load the fxml resource
        Parent root = null;
        try {
            root = loader.load();
        } catch (Exception e) {
            e.printStackTrace();
        } 
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}
