package edu.ntnu.idatt2001.oblig3.cardgame;

import edu.ntnu.idatt2001.oblig3.cardgame.GUI.CardgameGUI;
import javafx.application.Application;

/**
 * Main class that launches the GUI application.
 */
public class Main {
    public static void main(String[] args) {
        Application.launch(CardgameGUI.class,args);
    }
}
