package edu.ntnu.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;


/**
 * Class representing a deck of cards
 */
public class DeckOfCards {
    ArrayList<PlayingCard> deck = new ArrayList<>();
    private final char[] suit = {'S','H','D','C'};

    /**
     * Constructor that creates a standard deck of cards
     */
    public DeckOfCards() {
        for (char suit: suit) {
            for (int i = 1; i < 14; i++) {
                deck.add(new PlayingCard(suit,i));
            }
        }
    }

    /**
     * Method to deal a hand of cards from the deck
     * @param n Number of cards to be dealed
     * @return ArrayList with the PlayingCards in the hand
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        Random random = new Random();
        ArrayList<PlayingCard> hand = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int rand = random.nextInt(52-i);
            hand.add(deck.remove(rand));
        }
        return hand;
    }
}
