package edu.ntnu.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class representing a hand of playing cards
 */
public class HandOfCards {
    private ArrayList<PlayingCard> hand;

    /**
     * @param hand ArrayList of PlayingCards in the hand
     */
    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Getter for the hand variable
     * @return The hand as an ArrayList of PlayingCards
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }


    /**
     * Methods that calculates the sum of the faces in the hand
     * @return Sum of the faces in the hand
     */
    public int sumOfHand() {
        return hand.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();
    }


    /**
     * Methods that finds the cards of hearts in the hand
     * @return The hearts in the deck as a String
     */
    public String cardsOfHearts() {
        List<PlayingCard> hearts = hand.stream().filter(card -> card.getSuit() == 'H').collect(Collectors.toList());

        if (hearts.isEmpty()) {
            return "No hearts";
        } else {
            return hearts.stream().map(PlayingCard::getAsString).reduce((a,b) -> a + " " + b).get();
        }
    }


    /**
     * Checks if the queen of spades is part of the hand
     * @return If the queen of spades is part of the hand as boolean
     */
    public boolean queenOfSpades() {
        return hand.stream().anyMatch(card -> card.getAsString().equals("S12"));
    }


    /**
     * Checks if the hand is a flush
     * @return If the hand is a flush as boolean
     */
    public boolean flush() {
        List<PlayingCard> flush = hand.stream().filter(card -> card.getSuit() == hand.get(0).getSuit()).collect(Collectors.toList());
        return (flush.size() == 5);
    }
}
