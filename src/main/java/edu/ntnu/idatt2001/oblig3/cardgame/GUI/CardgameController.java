package edu.ntnu.idatt2001.oblig3.cardgame.GUI;

import edu.ntnu.idatt2001.oblig3.cardgame.DeckOfCards;
import edu.ntnu.idatt2001.oblig3.cardgame.HandOfCards;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Controller class for the FXML
 */
public class CardgameController {
    @FXML
    Button dealHand, checkHand;

    @FXML
    Label sum, cardsOfHearts, flush, queenOfSpades;

    @FXML
    Pane cards;

    @FXML
    Label card1, card2, card3, card4, card5;


    HandOfCards hand = null;


    /**
     * Method that deals a new hand of cards and shows in the GUI
     * When the deal hand button is clicked
     */
    @FXML
    public void deal() {
        dealHand.setOnAction((actionEvent) -> {
            Object[] labels = cards.getChildren().toArray();
            DeckOfCards deck = new DeckOfCards();
            hand = new HandOfCards(deck.dealHand(5));
            for (int i = 0; i < 5; i++){
                Label label = (Label) labels[i];
                label.setText(hand.getHand().get(i).getAsString());
            }
        });
    }

    /**
     * Method that checks different abilities of the hand and shows in the GUI
     * When the check hand button is clicked
     */
    @FXML
    public void check() {
        checkHand.setOnAction((actionEvent) -> {
            sum.setText(Integer.toString(hand.sumOfHand()));

            cardsOfHearts.setText(hand.cardsOfHearts());

            if (hand.flush()) {
                flush.setText("YES");
                flush.setBackground(new Background(new BackgroundFill(Color.GREEN,new CornerRadii(3), new Insets(0))));
            } else {
                flush.setText("NO");
                flush.setBackground(new Background(new BackgroundFill(Color.RED,new CornerRadii(3), new Insets(0))));
            }

            if (hand.queenOfSpades()) {
                queenOfSpades.setText("YES");
                queenOfSpades.setBackground(new Background(new BackgroundFill(Color.GREEN,new CornerRadii(3), new Insets(0))));
            } else {
                queenOfSpades.setText("NO");
                queenOfSpades.setBackground(new Background(new BackgroundFill(Color.RED,new CornerRadii(3), new Insets(-0))));
            }
        });
    }
}
